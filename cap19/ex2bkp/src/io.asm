section .data

        NULL equ 0

        SYS_read equ 0
        SYS_write equ 1
        SYS_open equ 2
        SYS_close equ 3

        STDOUT equ 1
        STDERR equ 2

        SUCCESS equ 0
        FAILURE equ 1

        FOUND_EOF equ 2

        TRUE equ 1
        FALSE equ 0

        LF equ 10

        LFStr db LF,NULL

        BUFSIZ equ 4096

        bufIndx dd BUFSIZ
        bufMax dd BUFSIZ

        errorStr db "getLine: Error occurred.",NULL

section .bss

        BUFFER resb BUFSIZ

section .text

;
; Print a NULL-terminated string
;
; Arguments:
;   1: fd, value
;   2: string, address
; Returns:
;   nothing
;
_printString:
	mov rdx, 0

	; Discover string length
checkChar:
	mov al, byte [rsi+rdx]
	cmp al, NULL
	je printString_done
	inc rdx
	jmp checkChar
printString_done:

	; Make write syscall
	; arg1: fd, arg2: string pointer, arg3: len
	mov rax, SYS_write
	; mov rsi, rdi ; string pointer
	; mov rdi, STDOUT ; fd
	; rdx already contains len
	syscall

	ret

; 
; rdi: fd,
; rsi: buffer
;
global writeString
writeString:
        call _printString
        ret

global printString
printString:
        mov rsi, rdi
        mov rdi, STDOUT
        call _printString
        ret

global printStringError
printStringError:
        mov rsi, rdi
        mov rdi, STDERR
        call _printString
        ret

;
; Function to print the command line arguments
;
; Arguments:
;   1: argc, value
;   2: argv, address of addresses
;
global printArguments
printArguments:
        push rbp
        mov rbp, rsp
	push r12
	push r13

        mov r12d, edi ; argc
        mov r13, rsi ; argv

        jmp checkCount

printCmdLineLoop:
        mov rdi, qword [r13] ; load address of next arg
        call printString

        ; print newline
        mov rdi, LFStr
        call printString

        add r13, 8 ; advance to next string
        dec r12d

checkCount:
        cmp r12d, 0
        jg printCmdLineLoop

	pop r13
	pop r12
        pop rbp
	ret

;
; Read a line of text
; Arguments: fd,
; buf -> buffer for the line
; maxlen -> size of the line buffer
;
; We also have our own internal buffer
;
; Algorithm:
;
; repeat {
;       if current_index >= buffer_maximum
;               read_buffer(buffer_size)
;               if error
;                       handle read error
;                       display error message
;                       exit routine (with nonzero)
;               reset pointers
;       get one character from buffer at current index
;       place character in text line buffer
;       increment current index
;       if character is LF
;               exit with zero (success)
; }
;
global getLine
getLine:
        push rbp
        mov rbp, rsp
        sub rsp, 4 ; line index
        push r15
        push r14
        push r13

        ; initialize line index to zero
        mov dword [rbp-4], 0

        mov r15, rdi ; fd
        mov r14, rsi ; buf
        mov r13, rdx ; maxLen

infiniteLoop:
        ; check if buffer non-empty
        mov eax, dword [bufIndx]
        cmp eax, BUFSIZ
        jl getChar

        ; refill our buffer
        mov rax, SYS_read
        mov rdi, r15 ; fd
        mov rsi, BUFFER ; buf
        mov rdx, BUFSIZ ; len
        syscall

        cmp rax, 0
        jl error
        je foundEof

        ; reset pointers
        mov dword [bufMax], eax ; save number of chars read
        mov dword [bufIndx], 0

getChar:
        ; ensure that we don't go past the EOF
        mov eax, dword [bufMax]
        mov edi, dword [bufIndx]
        cmp edi, eax
        jge foundEof

        ; get current buffer char
        mov rdi, BUFFER
        mov esi, dword [bufIndx]
        mov al, byte [rdi+rsi]

        mov ecx, dword [rbp-4] ; get line index

        mov byte [r14+rcx], al ; copy current character

        ; move buffer index forward
        inc esi
        mov dword [bufIndx], esi

        ; move line index forward
        inc ecx
        mov dword [rbp-4], ecx

        ; bound check line buffer
        ; needs to be LEN now (for NULL to fit, since we already incremented it)
        cmp ecx, r13d
        jge error

        ; is character LF
        cmp al, LF
        je appendNull

        jmp infiniteLoop

appendNull:
        mov ecx, dword [rbp-4] ; get line index
        mov dword [r14+rcx], NULL

        mov rax, SUCCESS

getLine_out:
        pop r13
        pop r14
        pop r15
        mov rsp, rbp
        pop rbp
        ret

error:
        mov rax, FAILURE
        jmp getLine_out

foundEof:
        mov rax, FOUND_EOF
        jmp getLine_out

;
; Thin wrappers for syscalls
;

global open
open:
        mov rax, SYS_open
        syscall
        ret

global read
read:
        mov rax, SYS_read
        syscall
        ret

global close
close:
        mov rax, SYS_close
        syscall
        ret

global write
write:
        mov rax, SYS_write
        syscall
        ret
