extern printString
extern pthread_create
extern pthread_join
extern itoa

; If any argument is passed to the program, it runs in parallel mode, otherwise runs in synchronously

section .data

        SUCCESS equ 0

        NULL equ 0

        TRUE equ 1

        LF equ 10

        SYS_exit equ 60

        NUMTHREADS equ 2

        qtd_loops dq 1000000
        x equ 1
        y equ 1

        parallel db 0

section .bss

        pthreadID0 resd 2

        total_str resb 10

        totals resq NUMTHREADS
        total resq 1

section .text

global main
main:
        push rbp
        mov rbp, rsp

        ; if we received any argument, do it in parallel
        cmp rdi, 2
        jl single
        mov byte [parallel], TRUE
single:

        mov rdi, pthreadID0
        mov rsi, NULL
        mov rdx, thread_function
        mov rcx, totals
        call pthread_create

        cmp byte [parallel], TRUE
        jne not_parallel
        lea rdi, [totals+8]
        call thread_function
not_parallel:

        mov rdi, qword [pthreadID0]
        mov rsi, NULL
        call pthread_join

        cmp byte [parallel], TRUE
        je yes_parallel
        lea rdi, [totals+8]
        call thread_function
yes_parallel:

        ; sum total
        mov rdi, totals
        mov rsi, NUMTHREADS
        mov rdx, 0
sum_loop:
        add rdx, qword [rdi+rsi*8]
        dec rsi
        cmp rsi, 0
        jge sum_loop
        mov qword [total], rdx

        mov rdi, qword [total]
        mov rsi, total_str
        call itoa

        mov rdi, total_str
        call printString

        mov rsp, rbp
        pop rbp
exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall

thread_function:
        mov rcx, qword [qtd_loops]
computeLoop:
        ; first arg (rdi) should point to the total destination for this thread
        mov rax, qword [rdi]
        cqo
        mov r8, x 
        div r8
        add rax, y
        mov qword [rdi], rax
        loop computeLoop

        ret