extern printString
extern pthread_create
extern pthread_join
extern itoa

; If any argument is passed to the program, it runs in parallel mode, otherwise runs in synchronously

section .data

        SUCCESS equ 0

        NULL equ 0

        TRUE equ 1

        LF equ 10

        SYS_exit equ 60

        NUMTHREADS equ 9

        qtd_loops dq 1000000
        x equ 1
        y equ 1

        parallel db 0

section .bss

        pthread_ids resq NUMTHREADS

        total_str resb 10

        totals resq NUMTHREADS
        total resq 1

section .text

global main
main:
        push rbp
        mov rbp, rsp
        ; keep the stack 16-byte aligned, otherwise we segfault
        sub rsp, 8
        push r15

        ; if we received any argument, do it in parallel
        cmp rdi, 2
        jl single_threaded

        mov r15, 0
spawn_thread:
        mov rdi, pthread_ids
        lea rdi, [rdi+r15*8]
        mov rsi, NULL
        mov rdx, thread_function
        mov rcx, totals
        lea rcx, [rcx+r15*8]
        call pthread_create

        inc r15
        cmp r15, NUMTHREADS
        jl spawn_thread

        mov r15, 0
wait_thread:
        mov rdi, pthread_ids
        mov rdi, qword [rdi+r15*8]
        mov rsi, NULL
        call pthread_join

        inc r15
        cmp r15, NUMTHREADS
        jl wait_thread

        ; sum total
        mov rdi, totals
        mov rsi, NUMTHREADS
        mov rdx, 0
sum_loop:
        add rdx, qword [rdi+rsi*8]
        dec rsi
        cmp rsi, 0
        jge sum_loop
        mov qword [total], rdx

print_result:
        mov rdi, qword [total]
        mov rsi, total_str
        call itoa

        mov rdi, total_str
        call printString

        pop r15
        mov rsp, rbp
        pop rbp
exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall

single_threaded:
        call synchronous_function
        jmp print_result

thread_function:
        mov rcx, qword [qtd_loops]
computeLoop:
        ; first arg (rdi) should point to the total destination for this thread
        mov rax, qword [rdi]
        cqo
        mov r8, x 
        div r8
        add rax, y
        mov qword [rdi], rax
        loop computeLoop

        ret

synchronous_function:
        mov rax, qword [qtd_loops]
        mov rdi, NUMTHREADS
        mul rdi
        mov rcx, rax
syncComputeLoop:
        mov rax, qword [total]
        cqo
        mov r8, x 
        div r8
        add rax, y
        mov qword [total], rax
        loop syncComputeLoop

        ret