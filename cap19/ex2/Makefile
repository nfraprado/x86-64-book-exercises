BIN := example
BUILD_DIR := build
SRC_DIR := src

SRCS := $(shell find $(SRC_DIR) -name *.asm -or -name *.c)

OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

$(BUILD_DIR)/$(BIN): $(OBJS)
	gcc -no-pie -lpthread -g -o $@ $^

$(BUILD_DIR)/%.asm.o: %.asm
	mkdir -p $(dir $@)
	yasm -g dwarf2 -f elf64 $< -o $@ -l $(@:.asm.o=.lst)

$(BUILD_DIR)/%.c.o: %.c
	mkdir -p $(dir $@)
	gcc -g -Wall -c $< -o $@

.PHONY: run interact clean 

run: $(BUILD_DIR)/$(BIN)
	gdb -q $(BUILD_DIR)/$(BIN) -ex 'source in_gdb.txt' -nh

interact: $(BUILD_DIR)/$(BIN)
	gdb -q $(BUILD_DIR)/$(BIN) -ex 'break *_start' -ex 'run'

clean:
	rm $(OBJS) $(BUILD_DIR)/$(BIN)

# Aliases
i: interact
r: run
