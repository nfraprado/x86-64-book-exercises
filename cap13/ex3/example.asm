;
; Implement a fileWrite function to write a string to a file
;

section .data

	SYS_read equ 0
	SYS_write equ 1
	SYS_open equ 2
	SYS_close equ 3
	SYS_exit equ 60
	SYS_creat equ 85 ; file create/open

	EXIT_SUCCESS equ 0

	STDIN equ 0
	STDOUT equ 1

	NULL equ 0
	LF equ 10 ; Line Feed (\n)

	FALSE equ 0
	TRUE equ 1

	SUCCESS equ 0
	NOSUCCESS equ 1

	S_IRUSR equ 400q
	S_IWUSR equ 200q
	S_IXUSR equ 100q

	STRING_IN_LEN equ 30

	LFStr db LF,NULL

	fileName db "password.txt",NULL

	password db "123",NULL

section .text

;
; Write a password string to a file
;
; Arguments:
;   1: file name string, address
;   2: password string, address
; Returns:
;   SUCCESS on success, NOSUCCESS on failure
;
global fileWrite
fileWrite:
	push rbp
	mov rbp, rsp
	push r13
	push r14

	mov r13, rsi ; password

	; Make create/open syscall
	; pathname, mode
	mov rax, SYS_creat
	; pathname already in rdi
	mov rsi, S_IRUSR | S_IWUSR ; mode
	syscall

	cmp rax, 0 ; check for error
	jl error
	mov r14, rax ; fd

	; Discover string length
	mov rdx, 0 ; length
checkChar:
	mov al, byte [r13+rdx]
	cmp al, NULL
	je done
	inc rdx
	jmp checkChar
done:

	; Make write syscall
	mov rax, SYS_write
	mov rdi, r14 ; fd
	mov rsi, r13 ; string
	; length already in rdx
	syscall

	cmp rax, 0
	jl error

	; Make close syscall
	mov rax, SYS_close
	mov rdi, r14 ; fd
	syscall
	cmp rax, 0
	jl error

	mov rax, SUCCESS
exitFileWrite:
	pop r14
	pop r13
	pop rbp
	ret

error:
	mov rax, NOSUCCESS
	jmp exitFileWrite

global _start
_start:

mainLoop:
	mov rdi, fileName
	mov rsi, password
	call fileWrite

	; exit with error value if unsuccessfull
	cmp rax, 0
	je exit
	mov rdi, rax
	jmp exitErr

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall

exitErr:
	mov rax, SYS_exit
	; error value already in rdi
	syscall
