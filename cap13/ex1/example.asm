;
; Implement a printString() function to write a string to the STDOUT
;

section .data

	SYS_exit equ 60
	SYS_write equ 1

	EXIT_SUCCESS equ 0

	STDOUT equ 1

	NULL equ 0
	LF equ 10

	FALSE equ 0
	TRUE equ 1

	string db "Hello World!",LF,NULL
	string2 db "Well Hello Friends! :^)",LF,NULL
	string3 db "Almost a SerenityOS",LF,NULL

section .text

;
; Function to print a NULL-terminated string to the terminal
;
; Arguments:
;   1: string, address
; Returns:
;   nothing
;
global printString
printString:
	mov rdx, 0

	; Discover string length
checkChar:
	mov al, byte [rdi+rdx]
	cmp al, NULL
	je done
	inc rdx
	jmp checkChar
done:

	; Make write syscall
	; arg1: fd, arg2: string pointer, arg3: len
	mov rax, SYS_write
	mov rsi, rdi ; arg2: string pointer
	mov rdi, STDOUT ; arg1: fd
	; rdx already contains len
	syscall

	ret

global _start

_start:
	mov rdi, string
	call printString

	mov rdi, string2
	call printString

	mov rdi, string3
	call printString

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
