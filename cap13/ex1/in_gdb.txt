# ---
# Debugger Input Script
# ---

break exit
run
set pagination off
set logging file out.txt
set logging overwrite
set logging on
set prompt
echo \n\n\n+------------------------------------------------------------+\n\n

echo \n\n+------------------------------------------------------------+\n\n\n
set logging off
quit
