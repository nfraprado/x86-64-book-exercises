;
; Implement a fileRead function to read a string from a file
;

section .data

	SYS_read equ 0
	SYS_write equ 1
	SYS_open equ 2
	SYS_close equ 3
	SYS_exit equ 60
	SYS_creat equ 85 ; file create/open

	EXIT_SUCCESS equ 0

	STDIN equ 0
	STDOUT equ 1

	NULL equ 0
	LF equ 10 ; Line Feed (\n)

	FALSE equ 0
	TRUE equ 1

	SUCCESS equ 0
	NOSUCCESS equ 1

	S_IRUSR equ 400q
	S_IWUSR equ 200q
	S_IXUSR equ 100q

	O_RDONLY equ 0q
	O_WRONLY equ 1q
	O_RDWR   equ 2q

	PASS_SZ_MAX equ 10

	LFStr db LF,NULL

	fileName db "password.txt",NULL

section .bss

	password resb PASS_SZ_MAX
	passLen resd 1

section .text

;
; Read a password string from a file
;
; Arguments:
;   1: file name string, address
;   2: password string destination, address
;   3: maximum password length, value
;   4: password length destination, address
; Returns:
;   SUCCESS on success, NOSUCCESS on failure
;
global fileRead
fileRead:
	push rbp
	mov rbp, rsp
	push r13
	push r14
	push r12
	push r15

	mov r13, rsi ; password destination
	mov r15, rcx ; length destination

	sub rdx, 1
	mov r12, rdx ; maximum length that can be read

	; Make open syscall
	mov rax, SYS_open
	; pathname already in rdi
	mov rsi, O_RDONLY ; flags
	syscall

	cmp rax, 0 ; check for error
	jl error
	mov r14, rax ; fd

	; Make read syscall
	mov rax, SYS_read
	mov rdi, r14 ; fd
	mov rsi, r13 ; string destination
	mov rdx, r12 ; length
	syscall

	cmp rax, 0
	jl error

	mov dword [r15], eax ; save length

	; Make close syscall
	mov rax, SYS_close
	mov rdi, r14 ; fd
	syscall

	cmp rax, 0
	jl error

	mov rax, SUCCESS
exitFileWrite:
	pop r15
	pop r12
	pop r14
	pop r13
	pop rbp
	ret

error:
	mov rax, NOSUCCESS
	jmp exitFileWrite

global _start
_start:

mainLoop:
	mov rdi, fileName
	mov rsi, password
	mov rdx, PASS_SZ_MAX
	mov rcx, passLen
	call fileRead

	; exit with error value if unsuccessfull
	cmp rax, 0
	je exit
	mov rdi, rax
	jmp exitErr

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall

exitErr:
	mov rax, SYS_exit
	; error value already in rdi
	syscall
