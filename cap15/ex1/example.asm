;
; Exec to a shell
;

section .data

	SYS_exec equ 59

	NULL equ 0

	shellStr db "/bin/sh", NULL

section .text

global _start
_start:

	mov rax, SYS_exec
	mov rdi, shellStr
	syscall
