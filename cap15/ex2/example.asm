;
; Implement a readString() function to get input from STDIN
; and a printString() function to write a string to the STDOUT
;

section .data

	SYS_read equ 0
	SYS_write equ 1
	SYS_exit equ 60

	EXIT_SUCCESS equ 0

	STDIN equ 0
	STDOUT equ 1

	NULL equ 0
	LF equ 10 ; Line Feed (\n)

	FALSE equ 0
	TRUE equ 1

	SUCCESS equ 0
	UNSUCCESS equ 1

	STRING_IN_LEN equ 16

	LFStr db LF,NULL

section .bss

	stringIn resb STRING_IN_LEN

section .text

;
; Function to print a NULL-terminated string to the terminal
;
; Arguments:
;   1: string, address
; Returns:
;   nothing
;
global printString
printString:
	mov rdx, 0

	; Discover string length
checkChar:
	mov al, byte [rdi+rdx]
	cmp al, NULL
	je done
	inc rdx
	jmp checkChar
done:

	; Make write syscall
	; arg1: fd, arg2: string pointer, arg3: len
	mov rax, SYS_write
	mov rsi, rdi ; string pointer
	mov rdi, STDOUT ; fd
	; rdx already contains len
	syscall

	ret

;
; Read string from input
;
; Arguments:
;   1: string, address
;   2: maximum length including NULL, value
; Returns:
;   number of characters read, not including the NULL
global readString
readString:
	push rbp
	mov rbp, rsp
	push rbx
	push r12
	push r13
	push r14

	mov rbx, 0 ; index in string
	mov r12, rdi ; string base address
	mov r14, FALSE ; overflow flag

	; mov r13, rsi ; total length
	; dec r13

inputReadLoop:
	; Check if there's space
; 	cmp rbx, r13
; 	jl notOverflow
; 	mov r14b, TRUE
; notOverflow:

	; Make read syscall
	; arg1: fd, arg2: string pointer, arg3: len
	mov rax, SYS_read
	mov rdi, STDIN ; fd
	lea rsi, [r12+rbx] ; current char pointer
	mov rdx, 1 ; len
	syscall

	cmp rax, 0
	je inputEOF

	cmp byte [r12+rbx], LF
	je inputLF

	; if we overflowed, don't increment the index, so we just swallow the
	; input
	; cmp r14b, TRUE
	; je inputReadLoop

	inc rbx
	jmp inputReadLoop

inputEOF:
	mov rax, 0 ; return zero to signify EOF
	jmp inputEnd
inputLF:
	mov rax, rbx ; return string read length

inputEnd:
	mov byte [r12+rbx], NULL

	pop r14
	pop r13
	pop r12
	pop rbx
	pop rbp
	ret

global readPrint
readPrint:
	push rbp
	mov rbp, rsp
	sub rsp, STRING_IN_LEN

	lea rdi, [rbp-STRING_IN_LEN]
	mov rsi, STRING_IN_LEN
	call readString
	cmp rax, 0 ; No characters read or EOF, quit
	je exitLoop

	; ; Otherwise echo back string with newline and repeat
	; lea rdi, [rbp-STRING_IN_LEN]
	; call printString
	; mov rdi, LFStr
	; call printString

	mov rax, SUCCESS
readPrintExit:
	mov rsp, rbp
	pop rbp
	ret

exitLoop:
	mov rax, UNSUCCESS
	jmp readPrintExit

global _start
_start:

mainLoop:
	call readPrint
	cmp rax, SUCCESS
	je mainLoop

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
