;
; Implement a macro to update a list to be x*2 for every x in the list
;

;
; Calculate average of list
;   <list> <len>
;
%macro listTimesTwo 2

	mov ecx, dword [%2] ; length
	mov rsi, 0 ; index
	mov ebx, 2

%%updateLoop:
	mov eax, dword [%1+rsi*4]
	mul ebx
	mov dword [%1+rsi*4], eax
	inc rsi
	loop %%updateLoop
%endmacro


section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	list dd 2, 8, 20, 3
	listLen dd 4

	list2 dd -1, -13, 27
	listLen2 dd 3

	list3 dd 6, 90
	listLen3 dd 2

section .text

global _start

_start:

	listTimesTwo list, listLen
	listTimesTwo list2, listLen2
	listTimesTwo list3, listLen3

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
