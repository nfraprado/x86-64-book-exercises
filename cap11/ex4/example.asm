;
; Convert integer to string (itoa), using a macro
;

;
; Convert integer dword to string (NULL terminated)
;   itoa <inInt>, <outString>
;
%macro itoa 2
	mov eax, dword [%1]
	mov cl, 0 ; count
	mov ebx, 10
%%digitLoop:
	mov edx, 0
	div ebx
	push rdx ; save remainder in stack. Use 64-bit register to be stack-friendly.
	inc cl
	cmp eax, 0
	jne %%digitLoop

	mov rsi, 0 ; index
%%charLoop:
	pop rdx
	add rdx, "0" ; Convert to ASCII
	mov byte [%2+rsi], dl
	inc rsi
	loop %%charLoop ; repeat until cl (digit count) is 0
	mov byte [%2+rsi], NULL
%endmacro

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	integer dd 9870
	integer2 dd 37
	integer3 dd 555

section .bss

	string resb 10
	string2 resb 10
	string3 resb 10

section .text

global _start

_start:
	itoa integer, string
	itoa integer2, string2
	itoa integer3, string3

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
