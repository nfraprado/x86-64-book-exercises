;
; Implement a macro to average a dword list of numbers
;

;
; Calculate average of list
;   <list> <len> <ave>
;
%macro aver 3

	mov ecx, dword [%2] ; length
	mov rsi, 0 ; index
	mov eax, 0 ; sum

%%numLoop:
	add eax, dword [%1+rsi*4]
	inc rsi
	loop %%numLoop

	cdq
	idiv dword [%2]
	mov dword [%3], eax

%endmacro

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	list dd 2, 8, 20, 3
	listLen dd 4
	ave dd 0

	list2 dd -1, -13, 27
	listLen2 dd 3
	ave2 dd 0

section .text

global _start

_start:
	aver list, listLen, ave
	aver list2, listLen2, ave2

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
