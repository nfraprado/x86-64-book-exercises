;
; Implement macros for average, minimum and maximum on dword lists
;

;
; Calculate average of list
;   <list> <len> <ave>
;
%macro aver 3

	mov ecx, dword [%2] ; length
	mov rsi, 0 ; index
	mov eax, 0 ; sum

%%sumLoop:
	add eax, dword [%1+rsi*4]
	inc rsi
	loop %%sumLoop

	cdq
	idiv dword [%2]
	mov dword [%3], eax
%endmacro

;
; Find minimum of list
;   <list> <len> <min>
;
%macro min 3
	mov ecx, dword [%2] ; length
	mov rsi, 0 ; index
	mov eax, dword [%1+rsi*4] ; initialize first minimum

%%iterate:
	mov ebx, dword [%1+rsi*4]
	cmp ebx, eax
	jge %%next
	mov eax, ebx ; save new min
%%next:
	inc rsi
	loop %%iterate

	mov dword [%3], eax
%endmacro

;
; Find maximum of list
;   <list> <len> <max>
;
%macro max 3
	mov ecx, dword [%2] ; length
	mov rsi, 0 ; index
	mov eax, dword [%1+rsi*4] ; initialize first maximum

%%iterate:
	mov ebx, dword [%1+rsi*4]
	cmp ebx, eax
	jle %%next
	mov eax, ebx ; save new min
%%next:
	inc rsi
	loop %%iterate

	mov dword [%3], eax
%endmacro

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	list dd 2, 8, 20, 3
	listLen dd 4
	aveRes dd 0
	minRes dd 0
	maxRes dd 0

	list2 dd -1, -13, 27
	listLen2 dd 3
	aveRes2 dd 0
	minRes2 dd 0
	maxRes2 dd 0

section .text

global _start

_start:
	aver list, listLen, aveRes
	aver list2, listLen2, aveRes2

	min list, listLen, minRes
	min list2, listLen2, minRes2

	max list, listLen, maxRes
	max list2, listLen2, maxRes2

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
