section .data

        SYS_exit equ 60
        SUCCESS equ 0

section .bss

        idt resb 10

section .text

global _start
_start:

        mov rdx, cr4
	sidt [idt]
