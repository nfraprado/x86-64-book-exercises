#include <linux/module.h>

#define IDT_SIZE 12

int task01_init(void)
{
	unsigned char idt_addr[IDT_SIZE];
	void *ptr;
	unsigned int i;

	asm("sidt %0 \n"
	    : "=m" (*idt_addr));
	for (i = 0; i < IDT_SIZE; i += 4)
		printk(KERN_DEBUG "%x %x %x %x\n", idt_addr[i], idt_addr[i+1], idt_addr[i+2], idt_addr[i+3]);

	ptr = *((void**)idt_addr);
	printk(KERN_DEBUG "%p\n", ptr);
	return 0;
}

void task01_exit(void)
{
}

module_init(task01_init)
module_exit(task01_exit)
MODULE_LICENSE("GPL");
