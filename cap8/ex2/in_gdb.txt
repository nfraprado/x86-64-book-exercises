# ---
# Debugger Input Script
# ---

break last
run
set pagination off
set logging file out.txt
set logging overwrite
set logging on
set prompt
echo \n\n\n+------------------------------------------------------------+\n\n

echo Sum:
x/uw &sum
echo Avg:
x/uw &avg
echo Min:
x/uw &min
echo Max:
x/uw &max
echo \n

echo \n\n+------------------------------------------------------------+\n\n\n
set logging off
quit
