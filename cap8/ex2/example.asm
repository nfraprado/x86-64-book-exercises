;
; Sum a list of numbers and retrieve the maximum, minimum and average
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	list db 50, 30, 25, 9, 80, 201, 255, 3
	listLen dd 8

section .bss

	sum resd 1
	min resd 1
	max resd 1
	avg resd 1

section .text

global _start

_start:
	mov cl, [listLen] ; set length
	mov rsi, 0 ; index = 0
	mov eax, 0 ;

	; Initialize min and max
	movzx ebx, byte [list]
	mov edx, ebx
	mov r8d, ebx
sumLoop:
	movzx ebx, byte [list+rsi]
	add eax, ebx ; accumulate the sum

	; Check if this is the new minimum
	cmp ebx, edx
	jae notMin
	mov edx, ebx
notMin:
	; Check if this is the new maximum
	cmp ebx, r8d
	jbe notMax
	mov r8d, ebx
notMax:
	inc rsi
	loop sumLoop

	; Save computed values
	mov dword [sum], eax
	mov dword [min], edx
	mov dword [max], r8d

	; Compute the average
	; eax already holds the sum
	mov edx, 0
	div dword [listLen]
	mov dword [avg], eax

	; Exit program succesfully
last:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
