;
; Given a list of numbers, find:
;     - minimum
;     - middle value
;     - maximum
;     - sum
;     - integer average
;     - For the negative numbers:
;         - sum
;         - count
;         - integer average
;     - For the numbers that are divisible by 3:
;         - sum
;         - count
;         - integer average
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	N equ 7
	list db 2, 2, -6, 11, 0, -10, -3
	listLen db N

	sum dd 0

	nCnt dd 0
	nSum dd 0

	tCnt dd 0
	tSum dd 0

	ddThree dd 3
	ddTwo dd 2

section .bss

	min resd 1
	mid resd 1
	max resd 1
	avg resd 1

	nAvg resd 1
	tAvg resd 1

section .text

global _start

_start:
	mov cl, byte [listLen]
	mov rsi, 0 ; index = 0

	; Initialize min and max with first value
	movzx eax, byte [list]
	mov dword [min], eax
	mov dword [max], eax
iteration:
	movsx eax, byte [list+rsi] ; get current value
	add dword [sum], eax ; add to sum

	cmp eax, dword [min]
	jge notMin
	mov dword [min], eax ; save as minimum
notMin:
	cmp eax, dword [max]
	jle notMax
	mov dword [max], eax ; save as maximum
notMax:
	cmp eax, 0
	jge notNegative

	; Add to negative stats
	add dword [nSum], eax
	inc dword [nCnt]
notNegative:
	mov ebx, eax ; Save current value for after the division
	cdq
	idiv dword [ddThree]
	cmp edx, 0
	jne notThreeMultiple

	; Add to divisible by three stats
	add dword [tSum], ebx
	inc dword [tCnt]
notThreeMultiple:
	inc rsi
	loop iteration

	; Get middle value
	mov al, byte [listLen]
	mov ah, 0
	div byte [ddTwo]
	cmp ah, 0 ; is listLen even?
	je even
	movzx rax, al
	movsx ebx, byte [list+rax]
	mov dword [mid], ebx ; Save middle value (odd size)
	jmp odd
even:
	movzx rax, al
	movsx ecx, byte [list+rax]
	movsx ebx, byte [list+rax-1]
	add ecx, ebx
	mov eax, ecx
	cdq
	movzx ebx, byte [ddTwo]
	idiv ebx
	mov dword [mid], eax ; Save mean of two middle values (even size)
odd:

	; Compute average
	mov eax, dword [sum]
	cdq
	movzx ebx, byte [listLen]
	idiv ebx
	mov dword [avg], eax

	; Compute negative average
	mov eax, dword [nSum]
	cdq
	idiv dword [nCnt]
	mov dword [nAvg], eax

	; Compute divisible by three average
	mov eax, dword [tSum]
	cdq
	idiv dword [tCnt]
	mov dword [tAvg], eax

	; Exit program successfully
last:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
