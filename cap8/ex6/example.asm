;
; Sort a list of numbers using the following bubble sort:
;   for ( i = (len-1) to 0 ) {
;       swapped = false
;       for ( j = 0 to i-1 ) {
;           if ( lst(j) > lst(j+1) ) {
;               tmp = lst(j)
;               lst(j) = lst(j+1)
;               lst(j+1) = tmp
;               swapped = true
;           }
;       }
;       if ( swapped = false ) exit
;   }
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	list dd 2, 2, -6, 11, 0, -10, -3
	listLen db 7

section .text

global _start

_start:
	mov cl, byte [listLen] ; i = (len - 1)
	sub cl, 1
outerLoop:
	mov dil, 0 ; swapped = false
	mov rsi, 0 ; j = 0
innerLoop:
	mov eax, [list+rsi*4] ; lst(j)
	mov ebx, [list+rsi*4+4] ; lst(j+1)
	cmp eax, ebx
	jle keep

	; Swap lst(j) and lst(j+1)
	mov [list+rsi*4], ebx
	mov [list+rsi*4+4], eax

	mov dil, 1 ; swapped = true
keep:
	inc rsi
	movzx r8, cl
	cmp rsi, r8 ; j == i - 1
	jne innerLoop

	cmp dil, 0 ; if ( swapped = false) exit
	je exit

	loop outerLoop

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
