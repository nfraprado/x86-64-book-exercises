;
; Sum a list of numbers
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	list db 50, 30, 25, 9
	listLen equ 4

section .bss

	sum resd 1

section .text

global _start

_start:
	mov cl, listLen ; set length
	mov rsi, 0 ; index = 0
	mov eax, 0 ;
sumLoop:
	movzx ebx, byte [list+rsi]
	add eax, ebx
	inc rsi
	loop sumLoop
	mov dword [sum], eax

	; Exit program succesfully
last:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
