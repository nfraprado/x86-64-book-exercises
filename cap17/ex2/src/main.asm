extern printArguments
extern printString
extern printStringError
extern writeString

extern getLine

extern read
extern open

extern atoi
extern itoa

section .data

        NULL equ 0

        SYS_write equ 1
        SYS_open equ 2
        SYS_exit equ 60
        SYS_close equ 3

        O_RDONLY equ 0
        O_WRONLY equ 1
        O_RDWR equ 2

        O_CREAT equ 100q
        O_EXCL equ 200q
        O_TRUNC equ 1000q
        O_APPEND equ 2000q

        S_IRUSR equ 400q
        S_IWUSR equ 200q

        SUCCESS equ 0
        FAILURE equ 1

        FOUND_EOF equ 2

        usageString db "Usage: <fileIn> <fileOut>",NULL

        ARGNUM equ 3 ; arg0 (executable name counts as one arg)

        LINEBUFSIZ equ 128

        LINENUMBUFSIZ equ 5

        spaceString db " ",NULL

section .bss

        lineBuffer resb LINEBUFSIZ
        lineNumberBuffer resb LINENUMBUFSIZ

section .text

global main
main:
        push rbp
        mov rbp, rsp
        sub rsp, 24
        push r15

        mov r15, 0 ; initialize line number

        cmp rdi, ARGNUM
        jne usage

        ; open input file
        mov rax, SYS_open
        mov rdi, qword [rsi+8] ; fetch argv[1]
        add rsi, 16
        mov rsi, qword [rsi]
        mov qword [rbp-24], rsi ; save argv[2]
        mov rsi, O_RDONLY
        syscall

        cmp rax, 0
        jl exitError

        mov qword [rbp-16], rax ; save input fd

        ; open output file
        mov rax, SYS_open
        mov rdi, qword [rbp-24]
        mov rsi, O_WRONLY | O_CREAT | O_TRUNC
        mov rdx, S_IRUSR | S_IWUSR
        syscall

        cmp rax, 0
        jl exitError

        mov qword [rbp-8], rax ; save output fd

getNextLine:
        mov rdi, qword [rbp-16]
        mov rsi, lineBuffer
        mov rdx, LINEBUFSIZ
        call getLine

        cmp rax, FAILURE
        je exitError

        cmp rax, FOUND_EOF
        je close

        ; convert line number
        mov rdi, r15
        mov rsi, lineNumberBuffer
        call itoa

        ; write line number
        mov rdi, qword [rbp-8]
        mov rsi, lineNumberBuffer
        call writeString

        mov rdi, qword [rbp-8]
        mov rsi, spaceString
        call writeString

        ; write line
        mov rdi, qword [rbp-8]
        mov rsi, lineBuffer
        call writeString

        inc r15
        jmp getNextLine

close:
        mov rax, SYS_close
        mov rdi, qword [rbp-16]
        syscall

        mov rax, SYS_close
        mov rdi, qword [rbp-8]
        syscall

exit:
        pop r15
        mov rsp, rbp
        pop rbp
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall

exitError:
        mov rax, SYS_exit
        mov rdi, FAILURE
        syscall

usage:
        mov rdi, usageString
        call printString
        jmp exitError