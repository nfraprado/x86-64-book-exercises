extern printArguments
extern printString
extern printStringError

extern getLine

extern read
extern open
extern close

extern atoi
extern itoa

section .data

        NULL equ 0

        SYS_open equ 2
        SYS_exit equ 60

        O_RDONLY equ 0
        O_WRONLY equ 1
        O_RDWR equ 2

        SUCCESS equ 0
        FAILURE equ 1

        FOUND_EOF equ 2

        usageString db "Usage: <filename>",NULL

        invalidNumString db "Invalid number: ",NULL

        ARGNUM equ 2 ; arg0 (executable name counts as one arg)

        LINEBUFSIZ equ 100

section .bss

        lineBuffer resb LINEBUFSIZ

section .text

global main
main:
        push rbp
        mov rbp, rsp

        cmp rdi, ARGNUM
        jne usage

        ; open file
        mov rax, SYS_open
        mov rdi, qword [rsi+8] ; fetch argv[1]
        push rdi ; save for later
        mov rsi, O_RDONLY
        syscall

        cmp rax, 0
        jl exitError

        push rax

getNextLine:
        mov rdi, qword [rsp]
        mov rsi, lineBuffer
        mov rdx, LINEBUFSIZ
        call getLine

        cmp rax, FOUND_EOF
        je exit
        cmp rax, FAILURE
        je exitError

        mov rdi, lineBuffer
        call printString

        jmp getNextLine

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall

exitError:
        mov rax, SYS_exit
        mov rdi, FAILURE
        syscall

usage:
        mov rdi, usageString
        call printString
        jmp exitError