;
; Determine if NULL terminated string representing a word is a palindrome
; Use the stack to reverse the word and then compare both
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	string db "anna",0
	output db 1

section .text

global _start

_start:
	mov rsi, 0
getLetter:
	mov al, byte [string+rsi]
	cmp al, 0
	je prepCompare
	movzx rax, al
	push rax
	inc rsi
	jmp getLetter
prepCompare:
	mov rsi, 0
compare:
	mov al, byte [string+rsi]
	cmp al, 0
	je exit ; if we reached 0, then exit and it is a palindrome
	inc rsi
	pop rbx
	cmp al, bl
	je compare ; if they're equal, continue comparing; if not, not a palindrome
	
	mov byte [output], 0 ; not a palindrome

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
