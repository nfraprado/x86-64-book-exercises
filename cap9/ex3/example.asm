;
; Determine if NULL terminated string representing a word is a palindrome.
; Ignore space, comma, dash and exclamation point. Be case-insensitive.
;
; Use the stack to reverse the word and then compare both
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	ascii_a equ 97
	ascii_A equ 65
	ascii_Z equ 90
	upperLowerDif equ ascii_a - ascii_A

	string db "A man, a plan, a canal - Panama!",0
	ignoredSymbols db " ,-!",0
	output db 1

section .text

global _start

; The iteration through the string will squeeze any of the ignoredSymbols
; present as well as convert uppercase to lowercase, in addition to pushing each
; of these characters to the stack so they can be compared to the cleaned up
; string later
_start:
	mov rsi, 0 ; string index
	; position where the next cleaned up char will be written to. Can be
	; lower than rsi if there were any of the skippedSymbols along the way
	mov rdx, 0
getLetter:
	mov al, byte [string+rsi]
	cmp al, 0
	je prepCompare

	; Check if one of the ignored symbols
	mov rdi, 0
getSymbol:
	mov bl, byte [ignoredSymbols+rdi]
	cmp bl, 0
	je notIgnored
	cmp al, bl
	je skipLetter
	inc rdi
	jmp getSymbol

notIgnored:
	; Check if uppercase letter
	cmp al, ascii_A
	jl saveLetter
	cmp al, ascii_Z
	jg saveLetter
	add al, upperLowerDif ; Convert to lowercase
saveLetter:
	movzx rax, al
	push rax
	mov byte [string+rdx], al ; Write cleaned up character in its new position
	inc rdx
	inc rsi
	jmp getLetter

skipLetter:
	; Update current char index but not next cleaned up char index (rdx)
	inc rsi 
	jmp getLetter

prepCompare:
	mov byte [string+rdx], 0 ; Write NULL at the new end of the string
	mov rsi, 0
compare:
	mov al, byte [string+rsi]
	cmp al, 0
	je exit ; if we reached 0, then exit and it is a palindrome
	inc rsi
	pop rbx
	cmp al, bl
	je compare ; if they're equal, continue comparing; if not, not a palindrome
	
	mov byte [output], 0 ; not a palindrome

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
