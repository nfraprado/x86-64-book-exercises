;
; Reverse a list of numbers using the stack
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	list dq 2, 2, -6, 11, 0, -10, -3
	listLen db 7

section .text

global _start

_start:
	mov cl, byte [listLen]
	mov rsi, 0
loop1:
	push qword [list+rsi*8]
	inc rsi
	loop loop1

	mov cl, byte [listLen]
	mov rsi, 0
loop2:
	pop qword [list+rsi*8]
	inc rsi
	loop loop2

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
