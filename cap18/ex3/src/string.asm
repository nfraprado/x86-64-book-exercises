;
; Check if %1 is a digit char. Result, 0 or 1, in %2.
;
%macro isdigit 2
	cmp %1, "0"
	jb %%not
	cmp %1, "9"
	ja %%not
	mov %2, TRUE
	jmp %%end
%%not:
	mov %2, FALSE
%%end:
%endmacro

section .data

	NULL equ 0

	FALSE equ 0
	TRUE equ 1

section .text

;
; Convert ASCII string to integer. Do error handling:
;   - return FALSE (= 0) if invalid
;   - return TRUE (= 1) if valid
;
;   atoi(&str, &int) -> rc
; I/O:     IN   OUT
; Reg:    rdi   rsi     rax
;
global atoi
atoi:
	mov rdx, 0 ; counter
	mov rcx, 0 ; clear register for char
	mov r9d, 0 ; sum
	mov r10b, FALSE ; negative

	mov cl, byte [rdi+rdx] ; get current char

	cmp cl, "-"
	je negative
	cmp cl, "+"
	je positive
	jmp digitLoop

negative:
	mov r10b, TRUE ; set negative flag
positive:
	inc rdx
digitLoop:
	mov cl, byte [rdi+rdx] ; get current char
	cmp cl, 0 ; if NULL, end conversion successfully
	je end

	isdigit cl, r8b
	cmp r8b, FALSE
	je error

	sub cl, "0" ; convert from ASCII to number
	imul r9d, 10

	; add or subtract digit depending on number being positive or negative
	cmp r10b, FALSE
	jne subtract
	add r9d, ecx
	jmp nextLoop
subtract:
	sub r9d, ecx
nextLoop:
	inc rdx
	jmp digitLoop

end:
	mov dword [rsi], r9d
	mov rax, FALSE
	ret

error:
	mov rax, TRUE
	ret

;
; Convert integer to string
;
; Args:
;  1) integer, value
;  2) string, address
;
; Note: this algorithm doesn't work with the maximum negative value (all 1's)
;
global itoa
itoa:
        mov ecx, 10 ; divider
        mov r9, 0 ; digit count
	mov r8, 0 ; negative flag

        cmp edi, 0
        jge prepDivideLoop

        neg edi ; make positive
	mov r8, TRUE

prepDivideLoop:
        mov eax, edi
divideLoop:
        mov edx, 0
        div ecx
        push rdx ; save remainder in stack
        inc r9
        cmp rax, 0
        jne divideLoop

        mov rdx, 0 ; string index

	cmp r8, TRUE
	jne stringLoop

	mov byte [rsi], "-"
	inc rdx
stringLoop:
        pop rax
        add al, "0" ; transform into ASCII
        mov byte [rsi+rdx], al
        inc rdx
        dec r9
        cmp r9, 0
        jne stringLoop

        mov byte [rsi+rdx], NULL

        ret