extern printString

section .data

        SUCCESS equ 0

        NULL equ 0

        LF equ 10

        SYS_exit equ 60

        targetSum dq 1.0
        sumStep dq 0.1

        equalStr db "Are the same.",LF,NULL
        notEqualStr db "Are not the same.",LF,NULL

section .text

global main
main:
        mov rcx, 10
        mov rax, 0
        cvtsi2sd xmm0, rax

sumLoop:
        addsd xmm0, qword [sumStep]
        loop sumLoop

        ucomisd xmm0, qword [targetSum]
        je equal

        mov rdi, notEqualStr
        call printString
        jmp exit

equal:
        mov rdi, equalStr
        call printString

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall