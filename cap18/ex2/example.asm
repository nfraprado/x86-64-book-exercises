section .data

        SYS_exit equ 60
        SUCCESS equ 0

        dZero dd 0.0
        dNegOne dd -1.0

        qZero dq 0.0
        qNegOne dq -1.0

        dValue dd 3.0
        dValue2 dd -4.0

        qValue dq 3.0
        qValue2 dq -4.0

section .bss

        dResult resd 1
        dResult2 resd 1

        qResult resq 1
        qResult2 resq 1

section .text

%macro fAbsf 1
        ucomiss %1, dword [dZero]
        ja %%done
        mulss %1, dword [dNegOne]
%%done:
%endmacro

%macro fAbsd 1
        ucomisd %1, qword [qZero]
        ja %%done
        mulsd %1, qword [qNegOne]
%%done:
%endmacro

global _start
_start:

        movss xmm0, dword [dValue]
        fAbsf xmm0
        movss dword [dResult], xmm0

        movss xmm0, dword [dValue2]
        fAbsf xmm0
        movss dword [dResult2], xmm0

        movsd xmm0, qword [qValue]
        fAbsd xmm0
        movsd qword [qResult], xmm0

        movsd xmm0, qword [qValue2]
        fAbsd xmm0
        movsd qword [qResult2], xmm0

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall