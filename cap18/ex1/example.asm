section .data

        SYS_exit equ 60
        SUCCESS equ 0

        list dq 1.3, 5.2, 9.5, 3.02
        listLen dw 4

section .bss

        sum resq 1
        ave resq 1

section .text

global _start
_start:

        mov rax, list
        mov ecx, dword [listLen]
sumLp:
        dec ecx
        addsd xmm0, qword [rax+rcx*8]
        cmp ecx, 0
        jne sumLp

        movsd qword [sum], xmm0

        cvtsi2sd xmm1, dword [listLen]
        divsd xmm0, xmm1
        movsd qword [ave], xmm0

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall