section .data
section .text

;
; Compute sum and average of list
;
;  stats(lst, len, &sum, &ave)
;
global stats
stats:
        mov r8, 0 ; index
        mov rax, 0 ; sum

lop:
        add eax, dword [rdi+r8*4]
        inc r8
        cmp r8, rsi
        jl lop

        mov dword [rdx], eax ; output sum

        cdq
        idiv esi

        mov dword [rcx], eax ; output ave

        ret