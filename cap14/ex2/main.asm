;
; Call stats function from a different file
;

extern lstAverage
extern lstSum

section .data

	SYS_exit equ 60

	EXIT_SUCCESS equ 0

	len dd 3
	lst dd 2, 5, -3

section .bss

	ave resd 1

section .text

global _start
_start:
	mov rdi, lst
	mov esi, dword [len]
	call lstAverage
	mov dword [ave], eax

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
