section .text

global stats

; stats function
; rdi: arr
; rsi: len
; rdx, &sum
; rcx: &ave
stats:
	push r11 ; prologue

	mov r11, 0 ; index
	mov rax, 0 ; sum

sumLoop:
	add eax, dword [rdi+r11*4]
	inc r11
	cmp r11, rsi
	jl sumLoop

	mov dword [rdx], eax ; output sum

	cdq ; just output sum, so we can override its address now
	idiv esi
	mov dword [rcx], eax ; output ave

	pop r11 ; epilogue
	ret
