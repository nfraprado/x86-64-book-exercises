extern void stats(int[], int, int *, int *);
extern void printString(char*);
extern void itoa(int, char*);

int main()
{
	int lst[] = {1, -2, 3, -4, 5, 7, 9, 11};
	int len = 8;
	int sum, ave;
	char sumStr[10];
	char aveStr[10];

	stats(lst, len, &sum, &ave);

	printString("Stats:\n");

	printString("  Sum = ");
	itoa(sum, sumStr);
	printString(sumStr);

	printString("\n");

	printString("  Ave = ");
	itoa(ave, aveStr);
	printString(aveStr);

	printString("\n");

	return 0;
}
