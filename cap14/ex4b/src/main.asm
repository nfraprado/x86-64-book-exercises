extern stats
extern printf
extern printString
extern itoa
global main

section .data

        SYS_exit equ 60
        EXIT_SUCCESS equ 0
        NULL equ 0

        lst dd 2,3,10
        lstLen dd 3
        fmt db "Sum: %s",10,NULL
        strin db "Sum: ",NULL
        bla db "15",NULL

section .bss

        sum resd 1
        ave resd 1

        sumStr resb 10

section .text

main:
        push rbp

        mov rdi, lst
        mov esi, dword [lstLen]
        mov rdx, sum
        mov rcx, ave
        call stats

        mov edi, dword [sum]
        mov rsi, sumStr
        call itoa

        mov rdi, strin
        call printString

        mov rdi, bla
        call printString

        pop rbp

        mov rax, 0
        ret

exit:
        mov rax, SYS_exit
        mov rdi, EXIT_SUCCESS
        syscall
