void stats(int lst[], unsigned int len, int *sum, int *ave)
{
        unsigned int i;
        int my_sum = 0;

        for (i = 0; i < len; i++) {
                my_sum += lst[i];
        }

        *sum = my_sum;

        *ave = my_sum / len;
}