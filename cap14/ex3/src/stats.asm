section .data
section .text

;
; Compute sum and average of list
;
;  lstAverage(lst, len) -> ave
;
global lstAverage
lstAverage:
        push rsi
        ; rdi and rsi are the same
        call lstSum
        pop rsi
        ; rax now contains the sum

        cdq
        idiv esi

        ; average already in rax
        ret

;
; Compute sum of list
;
;   lstSum(lst, len) -> sum
;
global lstSum
lstSum:
        mov r8, 0 ; index
        mov rax, 0 ; sum

lop:
        add eax, dword [rdi+r8*4]
        inc r8
        cmp r8, rsi
        jl lop

        ; sum already in rax
        ret