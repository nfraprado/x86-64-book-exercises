section .data

        NULL equ 0

        STDOUT equ 1

        ERROR equ -1

        FALSE equ 0
        TRUE equ 1

        SYS_write equ 1

section .text

;
; Print string to the terminal
;
; Args:
;  1) string, address
;
; Returns: error value
;
global printString
printString:
        mov rdx, 0 ; index

        ; Discover length
nextChar:
        cmp byte [rdi+rdx], NULL
        je print
        inc rdx
        jmp nextChar

print:
        ; Make write syscall
        mov rax, SYS_write
        mov rsi, rdi ; string
        mov rdi, STDOUT ; fd
        ; length already in rdx
        syscall

        cmp rax, 0
        jne printStringErr

        ret

printStringErr:
        mov rax, ERROR
        ret

;
; Convert integer to string
;
; Args:
;  1) integer, value
;  2) string, address
;
; Note: this algorithm doesn't work with the maximum negative value (all 1's)
;
global itoa
itoa:
        mov ecx, 10 ; divider
        mov r9, 0 ; digit count

        cmp rdi, 0
        jge divideLoop

        neg rdi ; make positive

divideLoop:
        mov rax, rdi
        mov edx, 0
        div ecx
        push rdx ; save remainder in stack
        inc r9
        cmp rax, 0
        jne divideLoop

        mov rdx, 0 ; string index
stringLoop:
        pop rax
        add rax, "0" ; transform into ASCII
        mov byte [rsi+rdx], al
        inc rdx
        dec r9
        cmp r9, 0
        jne stringLoop

        mov byte [rsi+rdx], NULL

        ret
