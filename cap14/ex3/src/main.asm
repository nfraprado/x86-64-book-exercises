;
; Call stats function from a different file
;

extern lstAverage
extern lstSum
extern printString
extern itoa

section .data

	SYS_exit equ 60

	EXIT_SUCCESS equ 0

	NULL equ 0

	len dd 3
	lst dd 2, 5, -3

	avePromptStr db "Average: ",NULL
	sumPromptStr db "Sum: ",NULL

	LFStr db 10,NULL

section .bss

	aveStr resb 10
	sumStr resb 10

section .text

global _start
_start:
	; compute average
	mov rdi, lst
	mov esi, dword [len]
	call lstAverage

	; convert average to string
	mov rdi, rax
	mov rsi, aveStr
	call itoa

	mov rdi, avePromptStr
	call printString

	; print average value
	mov rdi, aveStr
	call printString

	; print newline
	mov rdi, LFStr
	call printString

	; compute sum
	mov rdi, lst
	mov esi, dword [len]
	call lstSum

	; convert sum to string
	mov rdi, rax
	mov rsi, sumStr
	call itoa

	mov rdi, sumPromptStr
	call printString

	mov rdi, sumStr
	call printString

	mov rdi, LFStr
	call printString

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
