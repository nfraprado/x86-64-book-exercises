extern printArguments
extern printString
extern printStringError

extern read
extern open
extern close

extern atoi
extern itoa

section .data

        NULL equ 0

        SYS_exit equ 60

        O_RDONLY equ 0
        O_WRONLY equ 1
        O_RDWR equ 2

        SUCCESS equ 0
        UNSUCCESS equ 1

        usageString db "Usage: <num1> <num2> <num3>",NULL

        invalidNumString db "Invalid number: ",NULL

        numCount equ 3

        STRLEN equ 10

section .bss

        num resd 1
        sumString resb STRLEN

section .text

global main
main:
        push rbp
        mov rbp, rsp
        push r13
        push r14
        push r15

        ; Verify argument count
        cmp rdi, numCount+1
        jne usage

        mov r15, rsi ; argv

        mov r13, 0 ; arg counter

convertNum:
        lea rdx, [r15+r13*8+8]
        mov rdi, qword [rdx]
        mov rsi, num
        call atoi
        cmp rax, 0
        jne invalidNum

        add r14d, dword [num]

        inc r13
        cmp r13, numCount
        jl convertNum

        mov rdi, r14
        mov rsi, sumString
        call itoa

        mov rdi, sumString
        call printString

        pop r15
        pop r14
        pop r13
        mov rsp, rbp
        pop rbp

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall

exitError:
        mov rax, SYS_exit
        mov rdi, UNSUCCESS
        syscall

usage:
        mov rdi, usageString
        call printString
        jmp exitError

invalidNum:
        mov rdi, invalidNumString
        call printStringError

        ; refetch invalid number string
        lea rdx, [r15+r13*8+8]
        mov rdi, qword [rdx]
        call printStringError

        jmp exitError
