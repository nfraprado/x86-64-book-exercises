extern printArguments

section .data

        SYS_exit equ 60
        SUCCESS equ 0

section .text

global main
main:
        push rbp
        mov rbp, rsp

        call printArguments

        mov rsp, rbp
        pop rbp

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall