section .data

        NULL equ 0
        SYS_write equ 1
        STDOUT equ 1

        LF equ 10

        LFStr db LF,NULL

section .text

;
; Function to print a NULL-terminated string to the terminal
;
; Arguments:
;   1: string, address
; Returns:
;   nothing
;
global printString
printString:
	mov rdx, 0

	; Discover string length
checkChar:
	mov al, byte [rdi+rdx]
	cmp al, NULL
	je done
	inc rdx
	jmp checkChar
done:

	; Make write syscall
	; arg1: fd, arg2: string pointer, arg3: len
	mov rax, SYS_write
	mov rsi, rdi ; string pointer
	mov rdi, STDOUT ; fd
	; rdx already contains len
	syscall

	ret

;
; Function to print the command line arguments
;
; Arguments:
;   1: argc, value
;   2: argv, address of addresses
;
global printArguments
printArguments:
        push rbp
        mov rbp, rsp
	push r12
	push r13

        mov r12d, edi ; argc
        mov r13, rsi ; argv

        jmp checkCount

printCmdLineLoop:
        mov rdi, qword [r13] ; load address of next arg
        call printString

        ; print newline
        mov rdi, LFStr
        call printString

        add r13, 8 ; advance to next string
        dec r12d

checkCount:
        cmp r12d, 0
        jg printCmdLineLoop

	pop r13
	pop r12
        pop rbp
	ret