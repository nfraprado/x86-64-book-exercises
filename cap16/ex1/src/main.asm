extern printString

section .data

        SYS_exit equ 60
        SUCCESS equ 0

        NULL equ 0
        LF equ 10

        LFStr db LF,NULL

section .text

global main
main:
        push rbp
        mov rbp, rsp
        sub rsp, 12

        mov dword [rbp-12], edi ; save argc
        mov qword [rbp-8], rsi ; save argv

        jmp checkCount

printCmdLineLoop:
        mov rsi, qword [rbp-8]
        mov rdi, qword [rsi]
        call printString

        ; print newline
        mov rdi, LFStr
        call printString

        add qword [rbp-8], 8 ; advance to next string
        dec dword [rbp-12]

checkCount:
        cmp dword [rbp-12], 0
        jg printCmdLineLoop

        mov rsp, rbp
        pop rbp

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall