section .data

        NULL equ 0
        SYS_write equ 1
        STDOUT equ 1

section .text

;
; Function to print a NULL-terminated string to the terminal
;
; Arguments:
;   1: string, address
; Returns:
;   nothing
;
global printString
printString:
	mov rdx, 0

	; Discover string length
checkChar:
	mov al, byte [rdi+rdx]
	cmp al, NULL
	je done
	inc rdx
	jmp checkChar
done:

	; Make write syscall
	; arg1: fd, arg2: string pointer, arg3: len
	mov rax, SYS_write
	mov rsi, rdi ; string pointer
	mov rdi, STDOUT ; fd
	; rdx already contains len
	syscall

	ret