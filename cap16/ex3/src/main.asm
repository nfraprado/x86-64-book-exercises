;
; Open the file passed as the first command line argument and print its content
; to the screen.
;

extern printArguments
extern printString
extern printStringError

section .data

        NULL equ 0

        SYS_read equ 0
        SYS_open equ 2
        SYS_close equ 3
        SYS_exit equ 60

        O_RDONLY equ 0
        O_WRONLY equ 1
        O_RDWR equ 2

        SUCCESS equ 0
        UNSUCCESS equ 1

        BUFSIZ equ 256

        usageString db "Usage: <filename>",NULL

section .bss

        BUF resb BUFSIZ

section .text

global main
main:
        push rbp
        mov rbp, rsp
        push r13

        ; Verify argument count
        cmp rdi, 2
        jne usage

        ; fetch filename
        lea rdx, [rsi+8]
        mov rdi, qword [rdx]

        mov rax, SYS_open
        mov rsi, O_RDONLY
        syscall

        cmp rax, 0
        jl exitError
        mov r13, rax ; fd

        mov rax, SYS_read
        mov rdi, r13
        mov rsi, BUF
        mov rdx, BUFSIZ
        syscall

        cmp rax, 0
        jl close

        mov byte [BUF+rax], NULL

        mov rdi, BUF
        call printString

close:
        mov rax, SYS_close
        mov rdi, r13
        syscall

        pop r13
        mov rsp, rbp
        pop rbp

exit:
        mov rax, SYS_exit
        mov rdi, SUCCESS
        syscall

exitError:
        mov rax, SYS_exit
        mov rdi, UNSUCCESS
        syscall

usage:
        mov rdi, usageString
        call printString
        jmp exitError
