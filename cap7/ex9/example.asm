;
; Compute the n'th fibonacci number
;    (for 0 <= n <= 46)
;

section .data

SYS_exit equ 60
EXIT_SUCCESS equ 0

n equ 46
result dd 0

section .text

global _start

_start:
	mov ecx, 1 ; Output if n == 0 || n == 1
	mov eax, n
	cmp eax, 0
	je output
	cmp eax, 1
	je output
	mov edx, 2 ; current index
	mov ebx, 1 ; fibonacci(n-1)
	mov eax, 1 ; fibonacii(n-2)
	mov ecx, 0
fibonacci:
	mov ecx, ebx ; fibonacci(n) = fibonacci(n-1)
	add ecx, eax ;		    + fibonacci(n-2)
	cmp edx, n   ; Check if we're done
	je output
	mov eax, ebx ; fibonacci(n-2) <- fibonacci(n-1)
	mov ebx, ecx ; fibonacci(n-1) <- fibonacci(n)
	inc edx
	jmp fibonacci
output:
	mov dword [result], ecx
	

	; Exit program succesfully
last:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
