;
; Compute sum of squares from 1 to n -> (1 + 2 + ... + n)^2
;    (for 0 < n < 256)
;

section .data

SYS_exit equ 60
EXIT_SUCCESS equ 0

n equ 250
sum dd 0

section .text

global _start

_start:
	mov al, 0	; current val
	mov bl, n 	; counter
	mov eax, 0
	mov r11b, 0
	movzx r11d, r11b
addNextSquare:
	inc r11d	; val++
	add eax, r11d	; sum += val
	dec bl		; i--
	cmp bl, 0
	jne addNextSquare	; if (i == 0) break;
	mul eax
	mov dword [sum], eax

	; Exit program succesfully
last:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
