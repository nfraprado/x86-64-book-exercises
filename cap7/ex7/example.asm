;
; Compute sum of squares from 1 to n -> 1^2 + 2^2 + ... + n^2
;    (for 0 < n < 256)
;

section .data

SYS_exit equ 60
EXIT_SUCCESS equ 0

n equ 250
sum dd 0

section .text

global _start

_start:
	mov al, 0	; current val
	mov bl, n 	; counter
	mov ecx, dword [sum]
	mov eax, 0
addNextSquare:
	inc r11b	; val++
	mov al, r11b
	mul al		; val^2
	add ecx, eax	; sum += val^2
	dec bl		; i--
	cmp bl, 0
	jne addNextSquare	; if (i == 0) break;

	mov dword [sum], ecx

	; Exit program succesfully
last:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
