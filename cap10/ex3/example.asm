;
; Convert string to integer (atoi)
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	string db "41275",NULL

section .bss

	integer resd 1

section .text

global _start

_start:
	mov rsi, 0
	mov eax, 0
	mov ecx, 10

charLoop:
	mov bl, byte [string+rsi]
	cmp bl, 0
	je done
	sub bl, "0"
	movzx ebx, bl
	mul ecx
	add eax, ebx ; accumulate value
	inc rsi
	jmp charLoop
done:
	mov dword [integer], eax

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
