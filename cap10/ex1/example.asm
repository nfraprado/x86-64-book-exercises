;
; Convert integer to string (itoa)
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	integer dd 9870

section .bss

	string resb 10

section .text

global _start

_start:
	mov eax, dword [integer]
	mov cl, 0 ; count
	mov ebx, 10
digitLoop:
	mov edx, 0
	div ebx
	push rdx ; save remainder in stack. Use 64-bit register to be stack-friendly.
	inc cl
	cmp eax, 0
	jne digitLoop

	mov rsi, 0 ; index
charLoop:
	pop rdx
	add rdx, "0" ; Convert to ASCII
	mov byte [string+rsi], dl
	inc rsi
	loop charLoop ; repeat until cl (digit count) is 0
	mov byte [string+rsi], NULL

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
