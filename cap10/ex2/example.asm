;
; Convert integer to string considering signedness
;   Would be easier to just convert to positive upfront, but the exercise wants
;   idiv to be used...
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	integer dd -973

section .bss

	string resb 10

section .text

global _start

_start:
	mov eax, dword [integer]
	mov cl, 0 ; count
	mov ebx, 10

	cmp eax, 0
	jl negative
	mov byte [string], "+"
	jmp digitLoop
negative:
	mov byte [string], "-"
	mov r8, 1 ; set negative flag
digitLoop:
	cdq
	idiv ebx
	push rdx ; save remainder in stack. Use 64-bit register to be stack-friendly.
	inc cl
	cmp eax, 0
	jne digitLoop

	mov rsi, 1 ; index, skipping sign
charLoop:
	pop rdx

	cmp r8, 0
	je notNeg

	; Find the positive of the number
	not rdx
	inc rdx

notNeg:
	add rdx, "0" ; Convert to ASCII
	mov byte [string+rsi], dl
	inc rsi
	loop charLoop ; repeat until cl (digit count) is 0
	mov byte [string+rsi], NULL

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
