;
; Implement a selection sort function. Algorithm:
;
;   begin
;       for i = 0 to len-1
;           small = arr(i)
;           index = i
;           for j = i to len-1
;               if (arr(j) < small ) then
;                   small = arr(j)
;                   index = j
;               end_if
;           end_for
;           arr(index) = arr(i)
;           arr(i) = small
;       end_for
;   end_begin
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	arr dd 2, 16, 12
	len dd 3
	arr2 dd -1, -5, 80, 32
	len2 dd 4
	arr3 dd 5, 2, 90, 2, 7
	len3 dd 5

section .text

global selection_sort

; selection_sort(&arr, len)
;   rdi: &arr
;   rsi: len
selection_sort:
	mov ebx, 0 ; i = 0
outer_loop:
	mov edx, dword [rdi+rbx*4] ; small = arr(i)
	mov ecx, ebx ; index = i

	mov r8d, ebx ; j = i
inner_loop:
	mov r9d, dword [rdi+r8*4] ; load arr(j)
	cmp r9d, edx ; arr(j) > small ?
	jge keep

	mov edx, r9d
	mov ecx, r8d
keep:
	inc r8d
	cmp r8d, esi ; j < len ?
	jb inner_loop

	mov r8d, dword [rdi+rbx*4] ; arr(index) = arr(i)
	mov dword [rdi+rcx*4], r8d

	mov dword [rdi+rbx*4], edx ; arr(i) = small

	inc ebx
	cmp ebx, esi ; i < len?
	jb outer_loop
	
	ret

global _start

_start:
	mov rdi, arr ; pass array address as first parameter
	mov esi, dword [len] ; pass array length as second parameter
	call selection_sort

	mov rdi, arr2
	mov esi, dword [len2]
	call selection_sort

	mov rdi, arr3
	mov esi, dword [len3]
	call selection_sort

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
