;
; Implement a selection sort function. Algorithm:
;
;   begin
;       for i = 0 to len-1
;           small = arr(i)
;           index = i
;           for j = i to len-1
;               if (arr(j) < small ) then
;                   small = arr(j)
;                   index = j
;               end_if
;           end_for
;           arr(index) = arr(i)
;           arr(i) = small
;       end_for
;   end_begin
;
; And a stats function that receives the sorted array and returns  the minimum,
; median, maximum, sum and average.
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	arr dd 2, 16, 12
	len dd 3
	arr2 dd -1, -5, 80, 32
	len2 dd 4
	arr3 dd 5, 2, 90, 2, 7
	len3 dd 5

section .bss

	min resd 1
	med resd 1
	max resd 1
	aver resd 1
	sum resd 1

	min2 resd 1
	med2 resd 1
	max2 resd 1
	aver2 resd 1
	sum2 resd 1

	min3 resd 1
	med3 resd 1
	max3 resd 1
	aver3 resd 1
	sum3 resd 1

section .text

global selection_sort

; selection_sort(&arr, len)
;   I/O:          I/O   IN
;   Reg:          rdi  rsi
selection_sort:
	mov ebx, 0 ; i = 0
outer_loop:
	mov edx, dword [rdi+rbx*4] ; small = arr(i)
	mov ecx, ebx ; index = i

	mov r8d, ebx ; j = i
inner_loop:
	mov r9d, dword [rdi+r8*4] ; load arr(j)
	cmp r9d, edx ; arr(j) > small ?
	jge keep

	mov edx, r9d
	mov ecx, r8d
keep:
	inc r8d
	cmp r8d, esi ; j < len ?
	jb inner_loop

	mov r8d, dword [rdi+rbx*4] ; arr(index) = arr(i)
	mov dword [rdi+rcx*4], r8d

	mov dword [rdi+rbx*4], edx ; arr(i) = small

	inc ebx
	cmp ebx, esi ; i < len?
	jb outer_loop
	
	ret

global stats

; stats(&arr, len, &min, &med, &max, &sum, &aver)
;   I/O:  IN   IN   OUT   OUT   OUT   OUT    OUT
;   Reg: rdi  rsi   rdx   rcx    r8    r9 [rsp+8]
stats:
	mov ebx, dword [rdi] ; min is first element
	mov dword [rdx], ebx ; output min

	mov ebx, dword [rdi+rsi*4-4] ; max is last element
	mov dword [r8], ebx ; output max

	mov eax, esi
	dec eax
	mov edx, 0
	mov ebx, 2
	div ebx
	cmp edx, 0 ; (len-1) is even?
	je simple

	; med = (leftMiddle + rightMiddle) / 2
	mov ebx, dword [rdi+rax*4] ; load arr((len-1)/2)
	add ebx, dword [rdi+rax*4+4] ; load arr((len-1)/2+1)
	mov eax, ebx
	cdq
	mov ebx, 2
	idiv ebx
	mov dword [rcx], eax ; output med
	jmp preSum

simple:
	; med = middle
	mov ebx, dword [rdi+rax*4] ; load arr((len-1)/2)
	mov dword [rcx], ebx ; output med

preSum:
	mov ebx, 0 ; index
	mov eax, 0
sumLoop:
	add eax, dword [rdi+rbx*4]
	inc ebx
	cmp ebx, esi
	jl sumLoop

	mov dword [r9], eax ; output sum

	cdq
	idiv esi
	mov rbx, qword [rsp+8] ; get &aver
	mov dword [rbx], eax ; output aver

	ret

global _start

_start:
	mov rdi, arr ; pass array address as first parameter
	mov esi, dword [len] ; pass array length as second parameter
	call selection_sort

	mov rdi, arr
	mov esi, dword [len]
	mov rdx, min
	mov rcx, med
	mov r8, max
	mov r9, sum
	push aver
	call stats
	sub rsp, 8

	; Repeat for data 2
	mov rdi, arr2
	mov esi, dword [len2]
	call selection_sort

	mov rdi, arr2
	mov esi, dword [len2]
	mov rdx, min2
	mov rcx, med2
	mov r8, max2
	mov r9, sum2
	push aver2
	call stats
	sub rsp, 8

	; Repeat for data 3
	mov rdi, arr3
	mov esi, dword [len3]
	call selection_sort

	mov rdi, arr3
	mov esi, dword [len3]
	mov rdx, min3
	mov rcx, med3
	mov r8, max3
	mov r9, sum3
	push aver3
	call stats
	sub rsp, 8

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
