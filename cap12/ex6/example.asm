;
; Convert integer to string (itoa), using a void function
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	integer dd 9870
	integer2 dd 37
	integer3 dd -555

section .bss

	string resb 10
	string2 resb 10
	string3 resb 10

section .text

;
; Convert signed integer dword to right justified string (NULL terminated) of a
; fixed length
;
; len <= space - 1
;
;   itoa(int, &str, len)
; I/O:    IN   OUT   IN
; Reg:   rdi   rsi  rdx
;
global itoa
itoa:
	; prologue
	push rbp
	mov rbp, rsp
	sub rsp, 1 ; reserve byte for signal local var
	push rbx

	mov eax, edi ; move int to eax for arithmetic
	mov r11d, 10
	mov ebx, edx ; copy len

	mov byte [rsi+rbx], NULL
	dec ebx

	; check polarity of int and save the signal char in the local stack
	cmp eax, 0
	jl negative
	mov byte [rbp-1], "+"
	jmp digitLoop
negative:
	mov byte [rbp-1], "-"

digitLoop:
	cdq
	idiv r11d

	cmp byte [rbp-1], "+"
	je notNegative

	; make positive
	not dl 
	inc dl
notNegative:
	add dl, "0" ; Convert to ASCII

	mov byte [rsi+rbx], dl ; Output character
	dec ebx

	cmp ebx, -1
	jle error

	cmp eax, 0
	jne digitLoop

	; write signal character
	mov r10b, byte [rbp-1]
	mov byte [rsi+rbx], r10b
	dec ebx
	cmp ebx, -1
	je end

fillSpace:
	mov byte [rsi+rbx], " "
	dec ebx
	cmp ebx, -1
	jg fillSpace

end:
	; epilogue
	pop rbx
	mov rsp, rbp
	pop rbp
	ret
; The number doesn't fit in this space, considering the initial sign character
; ('+'/'-'). But this is a void function, so we can't report the error back
; directly. Just for fun, let's write 'E' at the first character to report the
; error.
error:
	mov byte [rsi], "E"
	ret

global _start

_start:
	mov edi, dword [integer]
	mov rsi, string
	mov edx, 5
	call itoa

	mov edi, dword [integer2]
	mov rsi, string2
	mov edx, 5
	call itoa
	mov edi, dword [integer3]
	mov rsi, string3
	mov edx, 5
	call itoa

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
