;
; Convert ASCII string to integer (atoi). Do error handling.
;

;
; Check if %1 is a digit char. Result, 0 or 1, in %2.
;
%macro isdigit 2
	cmp %1, "0"
	jb %%not
	cmp %1, "9"
	ja %%not
	mov %2, TRUE
	jmp %%end
%%not:
	mov %2, FALSE
%%end:
%endmacro

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	FALSE equ 0
	TRUE equ 1

	string db "123",NULL
	string2 db "-55",NULL
	string3 db "1A3",NULL

section .bss

	integer resd 1
	integer2 resd 1
	integer3 resd 1

	ret1 resb 1
	ret2 resb 1
	ret3 resb 1

section .text

;
; Convert ASCII string to integer. Do error handling:
;   - return FALSE (= 0) if invalid
;   - return TRUE (= 1) if valid
;
;   atoi(&str, &int) -> rc
; I/O:     IN   OUT
; Reg:    rdi   rsi     rax
;
global atoi
atoi:
	mov rdx, 0 ; counter
	mov rcx, 0 ; clear register for char
	mov r9d, 0 ; sum
	mov r10b, FALSE ; negative

	mov cl, byte [rdi+rdx] ; get current char

	cmp cl, "-"
	je negative
	cmp cl, "+"
	je positive
	jmp digitLoop

negative:
	mov r10b, TRUE ; set negative flag
positive:
	inc rdx
digitLoop:
	mov cl, byte [rdi+rdx] ; get current char
	cmp cl, 0 ; if NULL, end conversion successfully
	je end

	isdigit cl, r8b
	cmp r8b, FALSE
	je error

	sub cl, "0" ; convert from ASCII to number
	imul r9d, 10

	; add or subtract digit depending on number being positive or negative
	cmp r10b, FALSE
	jne subtract
	add r9d, ecx
	jmp nextLoop
subtract:
	sub r9d, ecx
nextLoop:
	inc rdx
	jmp digitLoop

end:
	mov dword [rsi], r9d
	mov rax, TRUE
	ret

error:
	mov rax, FALSE
	ret

global _start

_start:
	mov rdi, string
	mov rsi, integer
	call atoi
	mov byte [ret1], al

	mov rdi, string2
	mov rsi, integer2
	call atoi
	mov byte [ret2], al

	mov rdi, string3
	mov rsi, integer3
	call atoi
	mov byte [ret3], al

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
