;
; Implement the following function:
;   stats1(arr, len, sum, ave)
;   - arr: input, array of dword, passed by reference
;   - len: input, length of the array, passed by value, unsigned dword
;   - sum: output, sum, passed by reference
;   - ave: output, average, passed by reference
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	arr dd 2, 8, 3, 5
	len dd 4

section .bss

	sum resd 1
	ave resd 1

section .text

global stats1

; stats1 function
; rdi: arr
; rsi: len
; rdx, &sum
; rcx: &ave
stats1:
	push r11 ; prologue

	mov r11, 0 ; index
	mov rax, 0 ; sum

sumLoop:
	add eax, dword [rdi+r11*4]
	inc r11
	cmp r11, rsi
	jl sumLoop

	mov dword [rdx], eax ; output sum

	cdq ; just output sum, so we can override its address now
	idiv esi
	mov dword [ecx], eax ; output ave

	pop r11 ; epilogue
	ret

global _start
_start:
	mov rdi, arr
	mov esi, dword [len]
	mov rdx, sum
	mov rcx, ave
	call stats1

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
