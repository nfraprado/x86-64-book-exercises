;
; Implement the following function:
;   stats2(arr, len, min, med1, med2, max, sum, ave)
;   - arr: input, array of dword, passed by reference
;   - len: input, length of the array, passed by value, unsigned dword
;   - min: output, minimum, passed by value, unsigned dword
;   - med1: output, first median, passed by value, unsigned dword
;   - med2: output, second median (for odd-length arrays it's equal to med1), passed by value, unsigned dword
;   - max: output, maximum, passed by value, unsigned dword
;   - sum: output, sum, passed by reference
;   - ave: output, average, passed by reference
; The array is considered already ordered
;

section .data

	SYS_exit equ 60
	EXIT_SUCCESS equ 0

	NULL equ 0

	arr dd 2, 6, 12, 14
	len dd 4

section .bss

	min resd 1
	med1 resd 1
	med2 resd 1
	max resd 1
	sum resd 1
	ave resd 1

section .text

global stats1

; stats2 function
; rdi: arr
; rsi: len
; rdx, &min
; rcx: &med1
; r8: &med2
; r9: &max
; stack 1st: sum
; stack 2nd: ave
stats2:
	; prologue
	push rbp ; save rbp
	mov rbp, rsp ; point rbp to the rbp on stack
	push r11 ; push saved register

	mov ebx, dword [rdi]
	mov dword [rdx], ebx ; output min

	mov rax, rsi
	sub rax, 1
	mov ebx, dword [rdi+rax*4]
	mov dword [r9], ebx ; output max

	mov edx, 0
	mov ebx, 2
	div ebx
	mov ebx, dword [rdi+rax*4]
	mov dword [rcx], ebx ; output med1
	cmp edx, 1
	je even ; comparing len -1, so it's backwards

	mov dword [r8], ebx ; output med2 = med1
	jmp prepSum
even:
	inc rax
	mov ebx, dword [rdi+rax*4]
	mov dword [r8], ebx ; output med2

prepSum:
	mov rax, 0 ; sum
	mov r11, 0 ; index
sumLoop:
	add eax, dword [rdi+r11*4]
	inc r11
	cmp r11, rsi
	jl sumLoop

	mov rbx, qword [rbp+16] ; load address of sum
	mov dword [rbx], eax ; output sum

	cdq ; just output sum, so we can override its address now
	idiv esi
	mov rbx, qword [rbp+24] ; load address of ave
	mov dword [rbx], eax ; output ave

	; epilogue
	pop r11 ; restore saved register
	pop rbp ; restore rbp
	ret

global _start
_start:
	mov rdi, arr
	mov esi, dword [len]
	mov rdx, min
	mov rcx, med1
	mov r8, med2
	mov r9, max
	push ave ; push in reverse order
	push sum
	call stats2
	add rsp, 16 ; remove arguments from stack

	; Exit program successfully
exit:
	mov rax, SYS_exit
	mov rdi, EXIT_SUCCESS
	syscall
